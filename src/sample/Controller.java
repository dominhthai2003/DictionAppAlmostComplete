package sample;

import GoogleAPITranslate.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import voice.TTS;
import Fx.Dictionary;
import Fx.Word;
import TrieSearch.Trie;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class Controller {
    @FXML
    Button translate;

    @FXML
    TextArea enArea;

    @FXML
    TextArea viArea;

    @FXML
    public TextField textWord;

    @FXML
    ListView<String> recommendedWordList;

    ObservableList<String> observableList = FXCollections.observableArrayList();

    List<String> wordList = new ArrayList<>();

    private Trie trieSearch = new Trie();

    private Dictionary dictionary = new Dictionary();

    /**
     * Search a word.
     */
    @FXML
    public void buttonFind(MouseEvent event) {

        String insertWord = textWord.getText().toLowerCase(Locale.ROOT);

        Stage findStage = new Stage();
        findStage.setTitle("MEANING");
        findStage.initModality(Modality.APPLICATION_MODAL);

        Label phoneticLabel = new Label();
        Label meaningLabel = new Label();
        Label wordLabel = new Label();
        wordLabel.setText(insertWord);
        wordLabel.setFont(new Font("Comic Sans MS", 50));
        wordLabel.setAlignment(Pos.CENTER);
        wordLabel.setTextFill(Color.FORESTGREEN);

        phoneticLabel.setFont(new Font("Comic Sans MS", 20));
        phoneticLabel.setTextFill(Color.LIGHTGREEN);

        meaningLabel.setTextFill(Color.LAWNGREEN);
        meaningLabel.setFont(new Font("Comic Sans MS", 15));

        //Speak that word.
        Button speakButton = new Button();
        speakButton.setText("Speak");
        speakButton.setOnAction(event1 -> {
            TTS speech = new TTS(insertWord);
        });

        VBox findPane = new VBox(10);

        int i = dictionary.dictionaryLookUp(insertWord);
        if (i != -1) {
            phoneticLabel.setText(dictionary.getWordList().get(i).getPronunciation());
            meaningLabel.setText(dictionary.getWordList().get(i).getDefinition());
            findPane.getChildren().addAll(wordLabel, phoneticLabel, meaningLabel, speakButton);
        }
        else {
            meaningLabel.setText("Not found,maybe you wrote it wrong");
            findPane.getChildren().addAll(wordLabel, meaningLabel);
        }
        findPane.setPadding(new Insets(20, 20, 25, 35));

        Scene scene = new Scene(findPane);
        findStage.setScene(scene);
        findStage.setMinWidth(500);
        findStage.show();
    }

    /**
     * Remove a word.
     */
    @FXML
    public void buttonRemove(MouseEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Remove a word");

        Label inputWordLabel = new Label();
        inputWordLabel.setText("Enter the word");

        TextField inputWord = new TextField();
        inputWord.setPromptText("Enter a word you want to remove");

        Button removeConfirm = new Button();
        removeConfirm.setText("Remove");

        VBox removePane = new VBox();
        removePane.getChildren().addAll(inputWordLabel, inputWord, removeConfirm);

        removeConfirm.setOnAction(event1 -> {
            String s = inputWord.getText().toLowerCase(Locale.ROOT);

            int i = dictionary.removeWord(s);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("MESSAGE");
            if (i != -1) {
                alert.setContentText("Word removed successfully");
                trieSearch.remove(s);
                observableList.clear();

            } else {
                alert.setContentText("Word not found in dictionary");
            }
            alert.show();
        });
        Scene scene = new Scene(removePane, 300, 500);
        stage.setScene(scene);
        stage.setMinWidth(300);
        stage.show();
    }

    /**
     * Add a word.
     */
    @FXML
    public void buttonAdd(MouseEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Add a word");

        Label inputWordLabel = new Label();
        inputWordLabel.setText("Enter a word");

        TextField inputWord = new TextField();
        inputWord.setPromptText("Enter a word you want to add");

        Label phonetic = new Label();
        phonetic.setText("Phonetics");

        TextField phoneticInput = new TextField();
        phoneticInput.setPromptText("Enter the word's phonetic");

        Label detailLabel = new Label();
        detailLabel.setText("Detail");

        TextArea wordDetail = new TextArea();

        Button addConfirm = new Button();
        addConfirm.setText("Add");

        VBox addPane = new VBox();
        addPane.getChildren().addAll(inputWordLabel, inputWord, phonetic,
                phoneticInput, detailLabel, wordDetail, addConfirm);
        addConfirm.setOnAction(event1 -> {
            String s1 = inputWord.getText().toLowerCase(Locale.ROOT);
            String s2 = phoneticInput.getText();
            String s3 = wordDetail.getText();

            int i = dictionary.addWord(new Word(s1, s2, s3));
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("MESSAGE");
            if (i != -1) {
                trieSearch.insert(s1);
                observableList.clear();
                alert.setContentText("Word added successfully");
            } else {
                alert.setContentText("Word already existed");
            }
            alert.show();
        });
        Scene scene = new Scene(addPane, 300, 500);
        stage.setScene(scene);
        stage.setMinWidth(300);
        stage.show();
    }

    /**
     * Change a word definition
     */
    @FXML
    public void buttonChangeDefinition(MouseEvent event) {

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Change a word's definition");

        Label inputWordLabel = new Label();
        inputWordLabel.setText("Enter the word");

        TextField changeDefinitionWord = new TextField();
        changeDefinitionWord.setPromptText("Enter the word you want to change definition");

        Label definitionLabel = new Label();
        definitionLabel.setText("New definition");

        TextArea definitionDetail = new TextArea();

        Button changeConfirm = new Button();
        changeConfirm.setText("Change");

        VBox changePane = new VBox();
        changePane.getChildren().addAll(inputWordLabel, changeDefinitionWord,definitionLabel, definitionDetail, changeConfirm);

        changeConfirm.setOnAction(event1 -> {
            String s = changeDefinitionWord.getText().toLowerCase(Locale.ROOT);
            String s2 = definitionDetail.getText();

            int i = dictionary.changeDefinition(s, s2);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("MESSAGE");
            if (i != -1) {
                alert.setContentText("WORD DEFINITION CHANGE SUCCESSFULLY");
            } else {
                alert.setContentText("WORD NOT EXIST IN DICTIONARY");
            }
            alert.show();
        });
        Scene scene = new Scene(changePane, 300, 500);
        stage.setScene(scene);
        stage.setMinWidth(300);
        stage.show();
    }

    /**
     * Suggest words everytime a new character is typed.
     */
    @FXML
    public void keyRelease(KeyEvent event) {
        for (Word word : dictionary.getWordList()) {
          trieSearch.insert(word.getTargetWord());

        }

        String s = textWord.getText();
        List<String> a = trieSearch.autocomplete(s);

        observableList = FXCollections.observableList(wordList);
        System.out.println(observableList.size());
        recommendedWordList.setItems(observableList);
        recommendedWordList.setOnMouseClicked(event1 -> {
            String str = recommendedWordList.getSelectionModel().getSelectedItems().toString();
            str = str.substring(1, str.length() - 1);
            textWord.setText(str);
        });
        observableList.clear();
        observableList.addAll(a);
    }

    @FXML
    void handleAPI1(MouseEvent event) throws IOException {
        String s = Translator.translate("en", "vi", enArea.getText());
        viArea.setText(s);
    }

    @FXML
    void handleAPI2(MouseEvent event) throws IOException {
        String s = Translator.translate("vi", "en", enArea.getText());
        viArea.setText(s);
    }
}
